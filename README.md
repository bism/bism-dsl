# A DSL for BISM

This DSL for BISM is designed to facilitate the specification of program events to capture, the required information from these events, and the destination where these events should be consumed. It provides three main constructs: Pointcuts, Events, and Monitors. Pointcuts define the joinpoints to capture from program execution, Events encapsulate the extracted information from pointcuts, and Monitors specify where events will be processed. The DSL supports various BISM selectors for pointcuts, allowing fine-grained control over the program execution. Additionally, both static and dynamic context objects are available to provide contextual information during event processing. The provided code examples demonstrate the usage of these constructs and selectors within the DSL. Here is a quick example

```java
pointcut pc1 after MethodCall(* *.*List.iterator(..))

event e1("create",[getMethodReceiver,getMethodResult]) on pc1 

monitor m1{
    class: com.Monitor1,
    events: [e1 to receive(String, List)]
}    
```
The transformer uses the selector `afterMethodCall` to capture the return of an  `Iterator` created from a  `List.iterator()`  method call.
It uses the dynamic context object provided to retrieve the associated objects with the event, and pushes them into a list.
Then, invokes a monitor passing the extracted information.

## Table of Contents
 
Certainly! Here's an updated table of contents with links:

1. [Introduction](#a-dsl-for-bism)
2. [Pointcuts](#pointcuts)
   - [BISM Selectors supported by pointcuts](#bism-selectors-supported-by-pointcuts)
   - [Static Context](#static-context)
   - [Dynamic Context](#dynamic-context)
3. [Events](#events)
4. [Monitors](#monitors)
5. [How to Use](./tool/README.md)
6. [Examples](./samples/README.md)

 
## Pointcuts

Pointcuts allow you to specify the joinpoints to capture from program execution. Each pointcut has a name and one or more [BISM Selectors](#bism-selectors) associated with it. Here is an example of how pointcuts can be specified:

```dsl
pointcut pc_name BISM_Selector(pattern) with guard?
```

The pattern allows you to limit the scope of selectors to specific fields or methods by filtering on types, method signatures, or field names. Here are some examples:

- `* *.set*(..)` matches any method name that starts with "set"
- `int *.*.age` matches all fields with the name of "age" and type `int`.
- `float *.*(int, ..)` matches any method that returns a float and has its first parameter as an integer.

You can chain multiple selectors using the "||" to specify a disjunction of selectors. The "with guard" part is optional and can be used to specify a guard condition that must be satisfied for the pointcut to be triggered.  Only equality is supported. Guards are expressed over static context object associated with the selector. [Below](#static-context) are the static context objects for guards per selector. 

Here is an example of a pointcut  `pc0` that captures joinpoints before method calls to `Iterator` objects with  method name starting with `add` or `remove`, but only triggers when method calls starting with `add` have exactly 1 arguments.


```dsl
pointcut pc0 before MethodCall(* *.Iterator.add*(..)) with (getNumberOfArgs = 1)
                       || before MethodCall(* *.Iterator.remove*(..))
  ```                     


## Events

Events encapsulate the information that will be extracted from a pointcut. Each event must be associated with one pointcut and with its arguments. Here is how events can be specified:

```dsl
event event_name(event_args) on pc_name
```

Each argument can be either a single value or a list of values. A value can be a static or dynamic context, a string literal, a number, or a list.

The structure is as follows:

```
event_args ::= arg (, arg)*
arg ::= value | list
list ::= [ value (, value)* ]
value ::= static_context | dynamic_context | string_literal | number
```

Here is what each part of this structure means:

- `event_args`: The arguments for an event. This is a comma-separated list of arguments (`arg`).

- `arg`: An argument can either be a single value or a list of values.

- `list`: A list is a sequence of values enclosed in square brackets. The values within the list are separated by commas.

- `value`: A value can either be a static context, dynamic context, string literal, or number.

This structure allows for a flexible combination of different types of values to be passed as arguments to an event.  

Here is an example. Where the event extracts 2 lists, one with hardcoded string literals and the other with a string literal and a dynamic context object.

```dsl
event e3(["name", "list_object"], ["u",getMethodReceiver]) on pc0
```

## Monitors

Monitors monitor the occurrence of one or multiple events and specify where events will be extracted during execution. Here's how monitors can be specified:

```dsl
monitor m0 {
    class: monitor_class_name,
    events: [ event_name to method_name(args_types) ,  ... ]
}
```

You can associate the monitor directly with the event as well. Here is an example of how to do that:

```dsl
event e1("create",[getMethodReceiver,getMethodResult]) on pc1 
            to monitors.SafeListMonitor.receive(String,List)
```

You can also use the DSL to print an event to the terminal:

```dsl
event e1("create",[getMethodReceiver,getMethodResult]) on pc1 
                                  to console(String,List)
```


## BISM Selectors supported by pointcuts

BISM selectors facilitate the selection of joinpoints by associating each selector with a well-defined region in the bytecode, such as a single bytecode instruction, control-flow branch, or method call. The main available selectors are shown below:


* ```before Instruction```
executes before a bytecode instruction. If the instruction is the entry point of a basic block, the code executes after the instruction.

* ```after Instruction```
executes after a bytecode instruction. If the instruction is the exit point of a basic block, the code executes before the instruction.

* ```before MethodCall```
executes before a method call, after loading any needed values on the stack.

* ```after MethodCall```
executes immediately after a method call, before storing or popping the return value from the stack if any.
* ```before SetField``` 
executes when a value is assigned to a field.
* ```after GetField``` 
executes when a field's value is accessed or retrieved.

* ```on BasicBlockEnter```
executes after the entry Label of the basic block.
* ```on BasicBlockExit```
executes after the last instruction of a basic block; except when last instruction is a JUMP/RETURN/THROW instruction, then it executes before the last instruction.
* ```on TrueBranchEnter```
executes on the entry of the True successor block (applies only to conditional jumps).
* ```on FalseBranchEnter```
executes on the entry of the False successor block (applies only to conditional jumps).
* ```on MethodEnter```
executes on method entry block, rules of Before Basic Block apply here.
* ```on MethodExit```
executes on all exit blocks before the return or throw instruction.
 


## Supported Context Objects

Each selector provide a set of static and dynamic context object.

### Static Context

- **before SetField/after GetField**: `currentClassName`, `fieldOwner`, `fieldName`, `getOpcode`
- **on BasicBlockEnter/Exit,on True/FalseBranchEnter**: `id`, `size`, `getFirstRealInstruction`, `getLastRealInstruction`, `getSuccessors`, `getPredecessors`, `getSuccessorBlocks`, `getPredecessorBlocks`, `getTrueBranch`, `getFalseBranch`, `getFirst`, `getLast`
- **before/after Instruction**: `index`, `linenumber`, `opcode`, `className`, `basicBlock`, `methodName`, `stackOperandsCountIfConditionalJump`, `isConditionalJump`, `isBranchingInstruction`, `getBasicValueFrame`, `getSourceValueFrame`, `pretty`
- **on MethodEnter/Exit**: `name`, `className`, `signature`, `getBasicBlocks`, `getNumberOfBasicBlocks`, `getEntryBlock`, `getExitBlocks`, `getNumberOfArguments`, `getCFGEdges`
- **before/after MethodCall**: `currentClassName`, `methodOwner`, `methodName`, `name`, `returns`, `desc`, `getNumberOfArgs`

### Dynamic Context

- **before/after Instruction,on True/FalseBranchEnter**: `getThis`, `getThreadName`, `getThreadId`, `getStackValues`, `getArrayReadValue`, `getArrayWriteValue`
- **on GetField**: `getReadValue`, `getFieldOwnerInstance`
- **before/after MethodCall**: `getThis`, `getThreadName`, `getRandomUUID`, `getThreadId`, `getAllMethodArgs`, `getMethodResult`, `getMethodReceiver`
- **on MethodEnter/Exit**: `getThis`, `getThreadName`, `getRandomUUID`, `getThreadId`
- **on SetField**: `getWriteValue`, `getFieldOwnerInstance`
- **on BasicBlockEnter/Exit,on True/FalseBranchEnter**: `getThis`, `getThreadName`, `getThreadId`, `getArrayReadValue`, `getArrayWriteValue`



