pointcut pc0 before Instruction(* *.*(..)) with (isConditionalJump = true)
pointcut pc1 on TrueBranchEnter(* *.*(..))
pointcut pc2 on FalseBranchEnter(* *.*(..))

event e0([opcode,getStackValues]) on pc0 to console(List)
event e1("Entered true block") on pc1 to console(String)
event e2("Entered false block") on pc2 to console(String)