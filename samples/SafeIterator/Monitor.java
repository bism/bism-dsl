import java.util.*;
import transactionsystem.*;

public class Monitor {
    
    private static boolean initialised = false;
    private static HashSet<String> approvedAccounts = new HashSet<String>();
    private static HashSet<UserInfo> disabledUsers = new HashSet<UserInfo>();
    private static int eventsReceived = 0; // counter for events received
    private static boolean violationsOccurred = false; // boolean to check if violations occurred

    public static void checkInitialize() {
        initialised = true;
        eventsReceived++; // increment events received counter
    }
    
    public static void checkOpenSession() {
        if (!initialised) {
            fail("P2 violated");
        }
        eventsReceived++; // increment events received counter
    }
    
    public static void checkWithdraw(UserAccount a) {
        if (a.getBalance() < 0) {
            fail("P3 violated");
        }
        eventsReceived++; // increment events received counter
    }
    
    public static void checkDeposit(UserAccount a) {
        if (a.getBalance() < 0) {
            fail("P3 violated");
        }
        eventsReceived++; // increment events received counter
    }
    
    public static void checkApproveOpenAccount(List methodArgs) {
     
        if (approvedAccounts.contains((String) methodArgs.get(1))) {
            fail("P4 violated");
        } else {
            approvedAccounts.add((String) methodArgs.get(1));
        }
        eventsReceived++; // increment events received counter
    }
    
    public static void checkMakeDisabledUser(UserInfo u) {
        disabledUsers.add(u);
        eventsReceived++; // increment events received counter
    }

    public static void checkMakeActiveUser(UserInfo u) {
        disabledUsers.remove(u);
        eventsReceived++; // increment events received counter
    }

    public static void checkWithdrawFrom(UserInfo u) {
        if (disabledUsers.contains(u)) {
            fail("P5 violated");
        }
        eventsReceived++; // increment events received counter
    }
    
    public static void printStats() {
        if (violationsOccurred) {
            System.out.println("Violations occurred");
        } else {
            System.out.println("No violations reported");
        }
        System.out.println("Number of events received: " + eventsReceived);
    }
    
    private static void fail(String message) {
        System.err.println(message);
        // additional actions to take upon violation
        violationsOccurred = true; // set violationsOccurred to true
        eventsReceived++; // increment events received counter
    }

    public static void receiveEvent(String s, Iterator i) {
        eventsReceived++;
    }

    public static void receiveEvent(String s, List l) {
        eventsReceived++;
    }



    
}
