 
## Monitoring SafeIterator Property

The BISM specification shown below demonstrates how to intercept the creation of an iterator and various operations performed on it.

```java
pointcut pc1 after MethodCall(* *.*List.iterator(..))

pointcut pc2 before MethodCall(* *.Iterator.add*(..)) 
|| before MethodCall(* *.Iterator.remove*(..)) 
|| before MethodCall(* *.Iterator.set*(..))
|| before MethodCall(* *.Iterator.sort*(..)) 
|| before MethodCall(* *.Iterator.retain*(..)) 
|| before MethodCall(* *.Iterator.replace*(..)) 
|| before MethodCall(* *.Iterator.clear*(..));

event e1("create", [getMethodReceiver, getMethodResult]) on pc1 
event e2("update", getMethodReceiver) on pc2 

monitor m1 {
  class: Monitor,
  events: [e1 to receiveEvent(String, List),
           e2 to receiveEvent(String, Iterator)]
}
```

This BISM specification consists of two pointcuts, `pc1` and `pc2`, and two associated events, `e1` and `e2`. The `pc1` pointcut captures joinpoints after the method call to create an iterator on a list. The `pc2` pointcut captures joinpoints before various operations performed on the iterator, such as `add`, `remove`, `set`, `sort`, `retain`, `replace`, and `clear`.

The `e1` event logs the creation of an iterator along with the method receiver and result, while the `e2` event logs an update operation on the iterator along with the method receiver. 

