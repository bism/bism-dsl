pointcut pc1 after MethodCall(* *.*List.iterator(..))

pointcut pc2 before MethodCall(* *.Iterator.add*(..)) 
|| before MethodCall(* *.Iterator.remove*(..)) 
|| before MethodCall(* *.Iterator.set*(..))
|| before MethodCall(* *.Iterator.sort*(..)) 
|| before MethodCall(* *.Iterator.retain*(..)) 
|| before MethodCall(* *.Iterator.replace*(..)) 
|| before MethodCall(* *.Iterator.clear*(..))

event e1("create", [getMethodReceiver, getMethodResult]) on pc1 
event e2("update", getMethodReceiver) on pc2 

monitor m1 {
  class: Monitor,
  events: [e1 to receiveEvent(String, List),
           e2 to receiveEvent(String, Iterator)]
}