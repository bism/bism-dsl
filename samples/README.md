
# DSL Samples

This folder contains sample transformers written in the BISM DSL. It also contains FiTS application taken from [CRV 2014](https://link.springer.com/article/10.1007/s10009-017-0454-5#Sec36) as a base program to instrument.


 
## How to Run 


Navigate to the respective directory.

```
cd ./sample-name

```

You can customize the transformer and run the following to execute your transformer and instrument the sample application.
 

```
make run
```

Finally, if you want to clean up the sample, you can run the following command:

```
make clean
```

This will remove all the compiled files and restore the folder to its initial state.
