pointcut pc0 on MethodEnter(* *.*(..))
pointcut pc1 on BasicBlockEnter(* *.*(..))

event e0(getCFGEdges) on pc0 to console(List)
event e2(["entered block",id]) on pc1 to console(List)