

# The Tool

This folder contains `bism.jar` which includes the `DSL` extension packaged with `BISM`. It can be run in 2 modes: Load-time as a Java Agent, or Build-time Instrumentation. When run in load-time mode, the raw bytes are passed to BISM and instrumented before the linking phase. When run build-time mode, the source code is modified and written back to the files (always making a backup in the same path).
 


BISM can be run in 2 modes: Load-time as a Java Agent, or Build-time Instrumentation. When run in load-time mode, the raw bytes are passed to BISM and instrumented before the linking phase. When run build-time mode, the source code is modified and written back to the files (always making a backup in the same path).

## Run Args

To run BISM, you need to specify the run arguments.

- `specification` [required]: DSL-based specification file path **XOR**  `transformer`: compiled API-based transformer file path. Multiple specifications or transformers can be applied (,) separated.
- `generate` [optional]: Tells BISM to generate an API-based Java transformer file and a monitor class.
- `target` [required] (build-time only): File path to a compiled Java class or a Jar file to be instrumented. Example: `target=.../SimpleJavaProgram.jar`.
- `scope` [optional]: Specify packages, classes, and methods to be instrumented. Leave it empty to instrument all classes. Example: `scope=mypackage.*,package.Main.*,Test.main` (instruments all classes and methods under `mypackage`, all methods under `package.Main`, and method `main` in `Test` class).
- `blacklist` [optional]: Same as `scope`.
- `visualize` [optional]: Tells BISM to print HTML files representing the control flow graphs. Does not take any value.
- `output` [optional] (load-time only): Path to the directory for writing instrumented classes.
- `config` [optional]: Path to an XML file similar to the one in `examples/to-be-filled/bism.xml`.

## Config File

The config file allows you to specify all the above arguments (except `config`) in one file instead of specifying them separately on each run. The configuration file format is as follows:

```xml
<?xml version="1.0" encoding="utf-8" ?>
<bism>
    <config>
        <specification value="transformer.spec"/>
        <target value="pathtojarorclass"/>
        <scope value="mypackage.*"/> <!-- optional -->
        <blacklist value="myotherpackage.*"/> <!-- optional -->
        <visualize/> <!-- empty and optional -->
        <output value="/instrumented"/> <!-- optional -->
    </config>
</bism>
```

## Run

To run BISM, use the following commands:

### Build-time Instrumentation

With separate arguments:

```bash
java -jar bism.jar specification=transformer.spec:target=../SimpleJavaProgram.jar
```

Alternatively, for config files:

```bash
java -jar bism.jar config=build.xml
```

### Load-time Instrumentation

For example, to run as Java Agent (the target jar/class is provided as an argument to the `java` command, not BISM). With separate arguments:

```bash
java -javaagent:bism.jar=specification=transformer.spec -jar .../SimpleJavaProgram.jar
```

Alternatively, for config files:

```bash
java -javaagent:bism.jar=config=build.xml -jar .../SimpleJavaProgram.jar
```

Please note that you need Java 8 or above to run BISM.

 